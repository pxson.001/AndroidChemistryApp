package com.goku.android.iab.v3;

public enum PurchaseState
{
    PurchasedSuccessfully,
    Canceled,
    Refunded,
    SubscriptionExpired
}
