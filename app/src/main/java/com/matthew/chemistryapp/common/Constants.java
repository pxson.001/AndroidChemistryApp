package com.matthew.chemistryapp.common;

/**
 * Created by Son Pham on 10/11/2016.
 */
public class Constants {
    public static final String BASE_DATA_PATH = "data_v2/";
    public static final String BASE_LESSON_PATH = "lessons/";
    public static final String CONTENT_JSON = "/content.json";
    public static final String KEY_BILLING = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAgSvT9zhMorOCeEhZV3/Z7l0+nrjmEY0XFhk72DSdPlBpMCamUYTFLCwrhqY+6/0ieRGqNmhNBHhI4XVqpiDA58RAlgAI3nxYjpy7DDFKde/4BPzvAs2WF3217afG6DKgzu0cpd7oKNTq2gW4Ydz6DyksusF6aHp395wvpnvKQA/9Mh3a+X2PPgiSNYWhuRFKQcfB8naJrt/ptkfJmXZXwxnHtSFF0uuJ1O4oocd38ExdJGETCWhmN8rA4/JCXffwmBgHafNnRoyY+urIUuaiwI/clee09LdNkZ9rsfDaP9QqSWzNHYq+CNYnto68W+HVLxgEX7HZwm1pNBkWCnC0oQIDAQAB";
    public static final String BASE_PRODUCT_ID = "com.matthew.chemistryapp.unit.";
}
