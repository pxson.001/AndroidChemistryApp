package com.matthew.chemistryapp.fragment;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.matthew.chemistryapp.MyApp;
import com.matthew.chemistryapp.R;
import com.matthew.chemistryapp.model.Unit;
import com.matthew.chemistryapp.util.AESHelper;
import com.matthew.chemistryapp.util.FolderUtil;
import com.matthew.chemistryapp.model.Lesson;
import com.matthew.chemistryapp.model.Slide;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;

import butterknife.BindView;
import butterknife.ButterKnife;
import icepick.Icepick;
import icepick.State;

/**
 * Created by Son Pham on 10/9/2016.
 */
public class ContentFragment extends Fragment {
    public static final String UNIT = "activity_unit";
    public static final String LESSON = "activity_lesson";
    public static final String SLIDE = "slide";

    @State
    Unit unit;
    @State
    Lesson lesson;
    @State
    Slide slide;

    @BindView(R.id.image)
    ImageView imageView;

    public ContentFragment() {
    }

    public static Fragment newInstance(Unit unit, Lesson lesson, Slide slide) {
        Bundle args = new Bundle();
        args.putParcelable(UNIT, unit);
        args.putParcelable(LESSON, lesson);
        args.putParcelable(SLIDE, slide);
        ContentFragment fragment = new ContentFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Icepick.restoreInstanceState(this, savedInstanceState);

        Bundle arguments = getArguments();
        if (arguments != null) {
            unit = arguments.getParcelable(UNIT);
            lesson = arguments.getParcelable(LESSON);
            slide = arguments.getParcelable(SLIDE);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Icepick.saveInstanceState(this, outState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_content, container, false);
        ButterKnife.bind(this, view);

        setupImage(view);

        return view;
    }

    private void setupImage(View view) {
        loadPremiumImage();
    }

    private void loadPremiumImage() {
        InputStream inputStream = null;
        try {
            String imageRelativePath = FolderUtil.getSlidePath(
                    String.valueOf(unit.getId()),
                    String.valueOf(lesson.getId()),
                    String.valueOf(slide.getId()),
                    "png");

            String basePath = getActivity().getFilesDir().getAbsolutePath();
            File imageFile = new File(basePath + File.separator + imageRelativePath);
            InputStream imageInputStream = new FileInputStream(imageFile);
         //   Bitmap bitmap = MyApp.jealousSky.decryptToBitmap(imageInputStream);
            BitmapFactory.Options options=new BitmapFactory.Options();
            options.inSampleSize=2;
            Bitmap bitmap = BitmapFactory.decodeStream(imageInputStream,null,options);
            Drawable drawable = new BitmapDrawable(Resources.getSystem(), bitmap);

            imageView.setImageDrawable(drawable);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

}
