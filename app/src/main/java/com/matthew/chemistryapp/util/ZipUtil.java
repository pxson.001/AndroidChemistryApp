package com.matthew.chemistryapp.util;

import android.content.Context;
import android.util.Log;

import com.orhanobut.logger.Logger;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 * @author Son Pham
 */
public class ZipUtil {
    private File _zipFile;
    private InputStream _zipFileStream;
    private Context context;
    private String ROOT_LOCATION = "";
    private static final String TAG = "UNZIPUTIL";

    public ZipUtil(Context context, String destinationPath, File zipFile) {
        _zipFile = zipFile;
        this.context = context;
        this.ROOT_LOCATION = destinationPath;
        _dirChecker("");
    }

    public ZipUtil(Context context, String destinationPath, InputStream zipFile) {
        _zipFileStream = zipFile;
        this.context = context;
        this.ROOT_LOCATION = destinationPath;
        _dirChecker("");
    }

    public void unzip() {
        try {
            Log.i(TAG, "Starting to unzip");
            InputStream fin = _zipFileStream;
            if (fin == null) {
                fin = new FileInputStream(_zipFile);
            }
            ZipInputStream zin = new ZipInputStream(fin);
            ZipEntry ze = null;
            while ((ze = zin.getNextEntry()) != null) {
                Logger.d(TAG, "Unzipping " + ze.getName());

                if (ze.isDirectory()) {
                    _dirChecker(ROOT_LOCATION + "/" + ze.getName());
                } else {
                    FileOutputStream fout = new FileOutputStream(new File(ROOT_LOCATION, ze.getName()));
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    byte[] buffer = new byte[1024];
                    int count;

                    // reading and writing
                    while ((count = zin.read(buffer)) != -1) {
                        baos.write(buffer, 0, count);
                        byte[] bytes = baos.toByteArray();
                        fout.write(bytes);
                        baos.reset();
                    }

                    fout.close();
                    zin.closeEntry();
                }

            }
            zin.close();
            Logger.i(TAG, "Finished unzip");
        } catch (Exception e) {
            Logger.e(TAG, "Unzip Error", e);
        }

    }

    private void _dirChecker(String dir) {
        File f = new File(dir);
        Log.i(TAG, "creating dir " + dir);

        if (dir.length() >= 0 && !f.isDirectory()) {
            f.mkdirs();
        }
    }
}