package com.matthew.chemistryapp.util;

import android.content.Context;

import java.io.File;

/**
 * Created by Son Pham on 10/14/2016.
 */
public class FolderUtil {
    public static final String DATA_DIR = "data_v2";
    public static final String LESSONS_DIR = "lessons";
    public static final String EMPTY_FILE_NAME = "empty.html";

    public static boolean isDataExists(Context context) {
        String basePath = context.getFilesDir().getAbsolutePath();
        String dataPath = basePath + File.separator + DATA_DIR;
        File dataFolder = new File(dataPath);
        if (dataFolder.exists() && dataFolder.isDirectory()) {
            return true;
        }
        return false;
    }

    public static boolean isLessonExists(Context context) {
        String basePath = context.getFilesDir().getAbsolutePath();
        String lessonPath = basePath + File.separator + DATA_DIR + File.separator + LESSONS_DIR;
        File lessonsFolder = new File(lessonPath);
        if (lessonsFolder.exists() && lessonsFolder.isDirectory()) {
            return true;
        }
        return false;
    }

    public static boolean isUnitExists(Context context, int unitId) {
        String basePath = context.getFilesDir().getAbsolutePath();
        String unitPath = basePath
                + File.separator + DATA_DIR
                + File.separator + unitId;
        File unitFolder = new File(unitPath);
        if (unitFolder.exists() && unitFolder.isDirectory()) {
            return true;
        }
        return false;
    }

    public static boolean isLessonExists(Context context, int unitId, int lessonId) {
        String basePath = context.getFilesDir().getAbsolutePath();
        String lessonPath = basePath
                + File.separator + DATA_DIR
                + File.separator + unitId
                + File.separator + LESSONS_DIR
                + File.separator + lessonId;
        File lessonFolder = new File(lessonPath);
        if (lessonFolder.exists() && lessonFolder.isDirectory()) {
            return true;
        }
        return false;
    }

    public static String getSlidePath(String unitId, String lessonId, String slideId, String extension) {
        return DATA_DIR
                + File.separator + unitId
                + File.separator + LESSONS_DIR
                + File.separator + lessonId
                + File.separator + slideId
                + "." + extension;
    }

    public static String getEmptyPath() {
        return DATA_DIR
                + File.separator + EMPTY_FILE_NAME;

    }

    public static String getDataPath(Context context) {
        String basePath = context.getFilesDir().getAbsolutePath();
        String dataPath = basePath + File.separator + DATA_DIR;
        return dataPath;
    }

    public static String getUnitPath(Context context, String unitID) {
        String basePath = context.getFilesDir().getAbsolutePath();
        String dataPath = basePath + File.separator + DATA_DIR + File.separator + unitID;
        return dataPath;
    }

    public static String getLessonPath(String unitId, String lessonId) {
        return DATA_DIR
                + File.separator + unitId
                + File.separator + LESSONS_DIR
                + File.separator + lessonId;

    }

    public static String getContentPath(String lessonId) {
        return DATA_DIR
                + File.separator + LESSONS_DIR
                + File.separator + lessonId
                + File.separator + "content"
                + "." + "json";
    }

}
