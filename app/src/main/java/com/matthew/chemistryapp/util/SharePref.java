package com.matthew.chemistryapp.util;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Son Pham on 10/26/2016.
 */
public class SharePref {
    public static final String PREFNAME = "Nepure";
    public static final String GCM_ID = "GCM_ID";

    public final class Key {
        public static final String ID = "ID";
        public static final String PURCHASED_LIST = "purchaseList";
    }

    public static void putBoolean(Context context, String key, boolean value) {
        SharedPreferences prefi = context.getSharedPreferences(PREFNAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefi.edit();
        editor.putBoolean(key, value);
        editor.commit();
    }

    public static boolean getBoolean(Context context, String key, boolean defaultValue) {
        SharedPreferences sp = context.getSharedPreferences(PREFNAME, Context.MODE_PRIVATE);
        return sp.getBoolean(key, defaultValue);
    }

    public static void putInt(Context context, String key, int value) {
        SharedPreferences prefi = context.getSharedPreferences(PREFNAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefi.edit();
        editor.putInt(key, value);
        editor.commit();
    }

    public static int getInt(Context context, String key, int defaultValue) {
        SharedPreferences sp = context.getSharedPreferences(PREFNAME, Context.MODE_PRIVATE);
        return sp.getInt(key, defaultValue);
    }

    public static void putString(Context context, String key, String value) {
        SharedPreferences prefi = context.getSharedPreferences(PREFNAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefi.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public static String getString(Context context, String key, String defaultValue) {
        SharedPreferences sp = context.getSharedPreferences(PREFNAME, Context.MODE_PRIVATE);
        return sp.getString(key, defaultValue);
    }

    public static void putLong(Context context, String key, long value) {
        SharedPreferences prefi = context.getSharedPreferences(PREFNAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefi.edit();
        editor.putLong(key, value);
        editor.commit();
    }

    public static long getLong(Context context, String key, int defaultValue) {
        SharedPreferences sp = context.getSharedPreferences(PREFNAME, Context.MODE_PRIVATE);
        return sp.getLong(key, defaultValue);
    }

    public static boolean isLogin(Context context) {
        String user = getString(context, Key.ID, "");
        return (user != null && user.length() > 0);
    }
}
