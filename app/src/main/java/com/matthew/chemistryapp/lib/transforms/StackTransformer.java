package com.matthew.chemistryapp.lib.transforms;

import android.support.v4.view.ViewPager;
import android.view.View;

/**
 * Created by Son Pham on 10/9/2016.
 */
public class StackTransformer implements ViewPager.PageTransformer {
    @Override
    public void transformPage(View page, float position) {
        page.setTranslationX(page.getWidth() * -position);
        page.setTranslationY(position < 0 ? position * page.getHeight() : 0f);
    }
}
