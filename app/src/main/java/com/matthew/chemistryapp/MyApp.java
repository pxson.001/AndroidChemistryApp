package com.matthew.chemistryapp;

import android.app.Application;
import android.util.Log;

import com.github.cirorizzo.JealousSky;
import com.matthew.chemistryapp.model.Data;

import java.security.NoSuchAlgorithmException;

/**
 * Created by Son Pham on 10/15/16.
 */
public class MyApp extends Application {

    public static Data data;

    public static JealousSky jealousSky = JealousSky.getInstance();

    public static String view1;
    public static String view2;

    @Override
    public void onCreate() {
        super.onCreate();

        data = new Data();

        view1 = "7fc49d2486316025abb650623141da0f713d89371f5a81511579aba487348094";
        view2 = "d1c1468152c43a9e08281f511514fc1d1f4fa87162240df756f5500c8dbb54cb";

        try {
            jealousSky.initialize(view1, view2);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return;
        }
    }
}
