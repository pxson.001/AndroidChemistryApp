package com.matthew.chemistryapp.data;

import android.content.Context;
import android.util.Log;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.matthew.chemistryapp.MyApp;
import com.matthew.chemistryapp.common.Constants;
import com.matthew.chemistryapp.model.Data;
import com.matthew.chemistryapp.model.Lesson;
import com.matthew.chemistryapp.model.Slide;
import com.matthew.chemistryapp.model.Unit;
import com.matthew.chemistryapp.util.AESHelper;
import com.matthew.chemistryapp.util.FolderUtil;
import com.matthew.chemistryapp.util.ResourceUtils;
import com.orhanobut.logger.Logger;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Son Pham on 10/15/16.
 */
public class DataManager {

    public static final String APP_CONTENT = "units.json";

    public static final Gson gson = new Gson();

    public static Data getInitalizeData(Context context) throws IOException {
        String appContent = ResourceUtils.getFileFromAssets(context, Constants.BASE_DATA_PATH + Constants.CONTENT_JSON);
        return gson.fromJson(appContent, Data.class);
    }

    public static Data getData(Context context) throws Exception {
        String appContentPath = FolderUtil.getDataPath(context)
                + File.separator + APP_CONTENT;
        File appContentFile = new File(appContentPath);
        InputStream inputStream = new FileInputStream(appContentFile);
        //String decryptedContent = IOUtils.toString(MyApp.jealousSky.decrypt(inputStream), "utf-8");
        String decryptedContent = IOUtils.toString(inputStream, "utf-8");
        Logger.d("App content: %s", decryptedContent);
        return gson.fromJson(decryptedContent, Data.class);
    }

    public static Lesson getLesson(Context context, Lesson lesson) throws IOException {
        String lessonContentPath = context.getFilesDir().getAbsolutePath()
                + FolderUtil.getContentPath(String.valueOf(lesson.getId()));
        File lesosnContentFile = new File(lessonContentPath);
        String lessonContent = FileUtils.readFileToString(lesosnContentFile, "utf-8");
        return gson.fromJson(lessonContent, Lesson.class);
    }

    public static List<Slide> getSlides(String json) throws Exception {
        Lesson lesson = gson.fromJson(json, Lesson.class);
        return lesson.getSlides();
    }
    public static List<Lesson> getLessons(String json) throws Exception {
        Unit unit = gson.fromJson(json, Unit.class);
        return unit.getLessons();
    }

    public static void updateData(Context context, Data data) throws IOException {
        String appContentPath = FolderUtil.getDataPath(context)
                + File.separator + APP_CONTENT;
        File appContentFile = new File(appContentPath);
        String appContent = gson.toJson(data);
        FileUtils.writeStringToFile(appContentFile, appContent, "utf-8");
    }

}
