package com.matthew.chemistryapp.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Son Pham on 10/15/16.
 */
public class Data implements Parcelable {

    @SerializedName("name")
    private String name;
    @SerializedName("versionCode")
    private int versioncode;
    //    @SerializedName("lessons")
//    private List<Lesson> lessons;
    @SerializedName("units")
    private List<Unit> units;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getVersioncode() {
        return versioncode;
    }

    public void setVersioncode(int versioncode) {
        this.versioncode = versioncode;
    }

    public List<Unit> getUnits() {
        return units;
    }

    public void setUnits(List<Unit> units) {
        this.units = units;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
        dest.writeInt(this.versioncode);
        dest.writeTypedList(this.units);
    }

    public Data() {
    }

    protected Data(Parcel in) {
        this.name = in.readString();
        this.versioncode = in.readInt();
        this.units = in.createTypedArrayList(Unit.CREATOR);
    }

    public static final Parcelable.Creator<Data> CREATOR = new Parcelable.Creator<Data>() {
        @Override
        public Data createFromParcel(Parcel source) {
            return new Data(source);
        }

        @Override
        public Data[] newArray(int size) {
            return new Data[size];
        }
    };
}
