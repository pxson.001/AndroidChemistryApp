package com.matthew.chemistryapp.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Son Pham on 10/10/2016.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Slide implements Parcelable {
    @JsonProperty("id")
    private int id;
    @JsonProperty("name")
    private String name;
    @JsonProperty("hasNote")
    private boolean hasNote;

    public Slide() {
    }

    public Slide(int id, String name, boolean hasNote) {
        this.id = id;
        this.name = name;
        this.hasNote = hasNote;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isHasNote() {
        return hasNote;
    }

    public void setHasNote(boolean hasNote) {
        this.hasNote = hasNote;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.name);
        dest.writeByte(this.hasNote ? (byte) 1 : (byte) 0);
    }

    protected Slide(Parcel in) {
        this.id = in.readInt();
        this.name = in.readString();
        this.hasNote = in.readByte() != 0;
    }

    public static final Parcelable.Creator<Slide> CREATOR = new Parcelable.Creator<Slide>() {
        @Override
        public Slide createFromParcel(Parcel source) {
            return new Slide(source);
        }

        @Override
        public Slide[] newArray(int size) {
            return new Slide[size];
        }
    };
}
