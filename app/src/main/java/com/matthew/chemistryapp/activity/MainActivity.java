package com.matthew.chemistryapp.activity;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.goku.android.iab.v3.BillingProcessor;
import com.goku.android.iab.v3.TransactionDetails;
import com.matthew.chemistryapp.MyApp;
import com.matthew.chemistryapp.R;
import com.matthew.chemistryapp.adapter.ItemLessonAdapter;
import com.matthew.chemistryapp.common.Constants;
import com.matthew.chemistryapp.data.DataManager;
import com.matthew.chemistryapp.model.Unit;
import com.matthew.chemistryapp.util.FolderUtil;
import com.matthew.chemistryapp.model.Lesson;
import com.matthew.chemistryapp.util.NetworkUtils;
import com.matthew.chemistryapp.util.SharePref;
import com.orhanobut.logger.Logger;

import org.apache.commons.io.IOUtils;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;

import okhttp3.Call;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class MainActivity extends AppCompatActivity {
    public static final String LESSONS = "Lessons";
    public static final String MULTIPLE_CHOICE = "Multiple choice questions";
    public static final String UNIT = "unit";
    public static final String LECTURE = "lecture";
    private ListView mListView;
    private ItemLessonAdapter mAdapter;
    private Unit unit;
    private List<Lesson> lessons = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (getIntent() != null) {
            unit = getIntent().getParcelableExtra(ListUnitActivity.UNIT);
        }
        mListView = (ListView) findViewById(R.id.material_listview);
        getSupportActionBar().setTitle("Unit "+ unit.getId());
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        try {
            initLessons();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void initLessons() throws Exception {
        int countItem=0;
        int countQuestion=0;
        String basePath = getFilesDir().getAbsolutePath();
        String lessonPath = basePath + File.separator + Constants.BASE_DATA_PATH + unit.getId() + Constants.CONTENT_JSON;
        InputStream contentInputStream = new FileInputStream(lessonPath);
        String json = IOUtils.toString(contentInputStream, "utf-8");
        lessons = DataManager.getLessons(json);

        mAdapter = new ItemLessonAdapter(this);
        for(Lesson lesson: lessons){

            if (!lesson.getIspremium()&&countItem==0){
                Lesson lessonHeader1= new Lesson();
                lessonHeader1.setName(LESSONS);
                mAdapter.addSeparatorItem(lessonHeader1);
                countItem++;
            }
            if (lesson.getIspremium()&&countQuestion==0){
                Lesson lessonHeader2= new Lesson();
                lessonHeader2.setName(MULTIPLE_CHOICE);
                mAdapter.addSeparatorItem(lessonHeader2);
                countQuestion++;
            }
            mAdapter.addItem(lesson);
        }
        mListView.setAdapter(mAdapter);
        mAdapter.setOnOpenlistener(new ItemLessonAdapter.OnOpenLessonListener() {
            @Override
            public void onOpen(final Lesson lesson) {
                if (!LESSONS.equals(lesson.getName())&&!MULTIPLE_CHOICE.equals(lesson.getName())){
                if (!lesson.getIspremium()) {
                    Intent intent = new Intent(MainActivity.this, LessonActivity.class);
                    intent.putExtra(MainActivity.UNIT, unit);
                    intent.putExtra(MainActivity.LECTURE, lesson);
                    startActivity(intent);
                } else {
                    Intent intent = new Intent(MainActivity.this, QuestionActivity.class);
                    intent.putExtra(MainActivity.UNIT, unit);
                    intent.putExtra(MainActivity.LECTURE, lesson);
                    startActivity(intent);
                }
            }}
        });
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            default:
        }
        return super.onOptionsItemSelected(item);
    }

}
