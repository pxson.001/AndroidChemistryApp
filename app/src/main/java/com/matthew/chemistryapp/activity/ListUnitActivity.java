package com.matthew.chemistryapp.activity;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.goku.android.iab.v3.BillingProcessor;
import com.goku.android.iab.v3.TransactionDetails;
import com.matthew.chemistryapp.MyApp;
import com.matthew.chemistryapp.R;
import com.matthew.chemistryapp.adapter.ItemUnitAdapter;
import com.matthew.chemistryapp.common.Constants;
import com.matthew.chemistryapp.model.Unit;
import com.matthew.chemistryapp.util.FolderUtil;
import com.matthew.chemistryapp.util.NetworkUtils;
import com.matthew.chemistryapp.util.SharePref;
import com.orhanobut.logger.Logger;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;

import okhttp3.Call;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class ListUnitActivity extends AppCompatActivity implements BillingProcessor.IBillingHandler {
    public static final String UNIT = "unit";
    private ListView mListView;
    private ItemUnitAdapter mAdapter;
    private BillingProcessor bp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_unit);
        mListView = (ListView) findViewById(R.id.material_listview);
        initUnits();
        String base64EncodedPublicKey = Constants.KEY_BILLING;
        if (!BillingProcessor.isIabServiceAvailable(this)) {
            showToast("In-app billing service is unavailable, please upgrade Android Market/Play to version >= 3.9.16");
        }
        bp = new BillingProcessor(this, base64EncodedPublicKey, this);
    }

    private void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    @Override
    protected void onDestroy() {
        if (bp != null)
            bp.release();
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (!bp.handleActivityResult(requestCode, resultCode, data))
            super.onActivityResult(requestCode, resultCode, data);
    }

    private void initUnits() {
        List<Unit> units = MyApp.data.getUnits();
        mAdapter = new ItemUnitAdapter(this, units);
        mListView.setAdapter(mAdapter);
        mAdapter.setOnDownloadListener(new ItemUnitAdapter.OnDownloadUnitListener() {
            @Override
            public void onDownload(Unit unit, ImageButton downLoad) {
                if (NetworkUtils.isConnected(ListUnitActivity.this)) {
                    AsyncDownloader downloader = new AsyncDownloader(unit);
                    downloader.execute();
                } else {
                    showToast("Oops! Please check the Internet connection");
                }
            }
        });
        mAdapter.setOnOpenlistener(new ItemUnitAdapter.OnOpenUnitListener() {
            @Override
            public void onOpen(final Unit unit) {
                boolean isPremium = unit.ispremium();
                boolean isPurchased = SharePref.getBoolean(ListUnitActivity.this, Constants.BASE_PRODUCT_ID + unit.getId(), false);
                if (isPremium && !isPurchased) {
                    if (!isPurchased) {
                        MaterialDialog dialog = new MaterialDialog.Builder(ListUnitActivity.this)
                                .title("Do you want to buy this unit?")
                                .cancelable(false)
                                .positiveText("OK")
                                .onPositive(new MaterialDialog.SingleButtonCallback() {
                                    @Override
                                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                        if (NetworkUtils.isConnected(ListUnitActivity.this)) {
                                            bp.purchase(ListUnitActivity.this, Constants.BASE_PRODUCT_ID + unit.getId());
                                            Logger.d(Constants.BASE_PRODUCT_ID + unit.getId());
                                        } else {
                                            showToast("Oops! Please check the Internet connection");
                                        }
                                    }
                                })
                                .negativeText("Cancel")
                                .onNegative(new MaterialDialog.SingleButtonCallback() {
                                    @Override
                                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

                                    }
                                })
                                .show();
                    } else {
                        if (FolderUtil.isUnitExists(ListUnitActivity.this, unit.getId())) {
                            Intent intent = new Intent(ListUnitActivity.this, MainActivity.class);
                            intent.putExtra(ListUnitActivity.UNIT, unit);
                            startActivity(intent);
                        } else {
                            showToast("You need to download the unit first");
                        }
                    }
                } else {
                    if (FolderUtil.isUnitExists(ListUnitActivity.this, unit.getId())) {
                        Intent intent = new Intent(ListUnitActivity.this, MainActivity.class);
                        intent.putExtra(ListUnitActivity.UNIT, unit);
                        startActivity(intent);
                    } else {
                        showToast("You need to download the unit first");
                    }
                }
            }
        });
    }

    @Override
    public void onProductPurchased(String productId, TransactionDetails details) {
        SharePref.putBoolean(this, productId, true);
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void onPurchaseHistoryRestored() {

    }

    @Override
    public void onBillingError(int errorCode, Throwable error) {
        showToast("Error code: " + errorCode);
    }

    @Override
    public void onBillingInitialized() {

    }

    class AsyncDownloader extends AsyncTask<Void, Long, Boolean> {
        private String unitID;
        private String URL = "file_url";
        boolean showMinMax = false;
        MaterialDialog dialog;

        public AsyncDownloader(Unit unit) {
            this.unitID = String.valueOf(unit.getId());
            URL = "https://api.backendless.com/2F1846AD-FD0B-5A72-FFF3-AEB12DCE0800/v1/files/chemistry-ios-v2/" + unitID + ".zip";
            dialog = new MaterialDialog.Builder(ListUnitActivity.this)
                    .title("Downloading")
                    .content(unit.getName())
                    .progressNumberFormat("%d/%d kB")
                    .progress(false, 0, showMinMax)
                    .cancelable(false)
                    .show();
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            OkHttpClient httpClient = new OkHttpClient.Builder()
                    .connectTimeout(120, TimeUnit.SECONDS)
                    .writeTimeout(120, TimeUnit.SECONDS)
                    .readTimeout(120, TimeUnit.SECONDS)
                    .build();
            Call call = httpClient.newCall(new Request.Builder().url(URL).get().build());
            try {
                Response response = call.execute();
                if (response.code() == 200) {
                    InputStream inputStream = null;
                    OutputStream outputStream = null;
                    try {
                        inputStream = response.body().byteStream();
                        String dataPath = FolderUtil.getDataPath(ListUnitActivity.this);
                        outputStream = new FileOutputStream(new File(dataPath + File.separator + unitID + ".zip"));

                        byte[] buff = new byte[1024 * 4];
                        long downloaded = 0;
                        long target = response.body().contentLength();

                        publishProgress(0L, target * 2);
                        while (true) {
                            int readed = inputStream.read(buff);
                            if (readed == -1) {
                                break;
                            }
                            //write buff
                            downloaded += readed;
                            publishProgress(downloaded, target * 2);

                            outputStream.write(buff, 0, readed);
                            if (isCancelled()) {
                                return false;
                            }
                        }

                        if (outputStream != null) {
                            outputStream.close();
                        }


                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                dialog.setTitle("Unziping");
                            }
                        });

                        // Unzip
                        Logger.d("Creating temp zip file");
                        String outputFolder = dataPath;
                        String zipFileName = dataPath + File.separator + unitID + ".zip";
                        long progressCount = target;

                        Logger.d("Starting unzip");
                        InputStream fin = null;
                        BufferedInputStream bis = null;
                        if (fin == null) {
                            fin = new FileInputStream(new File(zipFileName));
                            bis = new BufferedInputStream(fin);
                        }
                        ZipInputStream zin = new ZipInputStream(bis);
                        ZipEntry ze = null;

                        ZipFile zipFile = new ZipFile(new File(zipFileName));
                        int numOfEntries = zipFile.size();
                        int progressUnit = (int) target / numOfEntries;

                        while ((ze = zin.getNextEntry()) != null) {
                            Log.d("Unzip activity_lesson", "Unzipping " + ze.getName());

                            if (ze.isDirectory()) {
                                _dirChecker(outputFolder + "/" + ze.getName());
                            } else {
                                FileOutputStream fout = new FileOutputStream(new File(outputFolder, ze.getName()));
                                BufferedOutputStream bos = new BufferedOutputStream(fout);
                                byte[] buffer = new byte[1024 * 8];
                                int count;

                                // reading and writing
                                while ((count = zin.read(buffer)) != -1) {
                                    bos.write(buffer, 0, count);
                                }

                                bos.close();
                                fout.close();
                                zin.closeEntry();
                            }

                            progressCount += progressUnit;
                            publishProgress(progressCount, target * 2);

                        }
                        zin.close();

                        Logger.d("Deleting temp zip file");
                        File deleteZipFile = new File(zipFileName);
                        deleteZipFile.delete();
                        Logger.d("Done");

                        return downloaded == target;
                    } catch (IOException ignore) {
                        return false;
                    } finally {
                        if (inputStream != null) {
                            inputStream.close();
                        }
                        if (outputStream != null) {
                            outputStream.close();
                        }
                    }


                } else {
                    return false;
                }
            } catch (IOException e) {
                return false;
            }
        }


        @Override
        protected void onProgressUpdate(Long... values) {
            dialog.setMaxProgress(values[1].intValue() / 1024);
            dialog.setProgress(values[0].intValue() / 1024);
        }

        @Override
        protected void onPostExecute(Boolean result) {
            if (result != null && result.booleanValue()) {
                dialog.setContent(result ? "Downloaded" : "Failed");
                mAdapter.notifyDataSetChanged();
            } else {
                showToast("Error when downloading the lecture");
            }

            dialog.dismiss();
        }

        private void _dirChecker(String dir) {
            File f = new File(dir);
            Log.i("Hihi", "creating dir " + dir);

            if (dir.length() >= 0 && !f.isDirectory()) {
                f.mkdirs();
            }
        }
    }
}
