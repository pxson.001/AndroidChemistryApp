package com.matthew.chemistryapp.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.matthew.chemistryapp.MyApp;
import com.matthew.chemistryapp.R;
import com.matthew.chemistryapp.util.AssetsUtil;
import com.matthew.chemistryapp.data.DataManager;
import com.matthew.chemistryapp.util.ZipUtil;
import com.matthew.chemistryapp.util.FolderUtil;
import com.orhanobut.logger.Logger;

import java.io.File;
import java.io.IOException;

/**
 * Created by Son Pham on 10/15/16.
 */

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                if (!FolderUtil.isDataExists(SplashActivity.this)) {
                    Logger.d("copyDataToCard");
                    copyDataToCard();
                }

                try {
                    Logger.d("initializeData from card");
                    initializeDataFromCard();
                } catch (IOException e) {
                    e.printStackTrace();
                    Logger.e("Error when initializing data from card");
                    Toast.makeText(SplashActivity.this, "Error when initializing data from card", Toast.LENGTH_SHORT).show();
                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(SplashActivity.this, "Error when initializing data from card", Toast.LENGTH_SHORT).show();
                }


                Intent i = new Intent(SplashActivity.this, ListUnitActivity.class);
                startActivity(i);

                finish();
            }
        }, 3000);
    }

    public void initializeDataFromCard() throws Exception {
        MyApp.data = DataManager.getData(this);
    }

    public void copyDataToCard() {
        if (!FolderUtil.isDataExists(this)) {
            String basePath = getFilesDir().getAbsolutePath();
            AssetsUtil.copyFileFromAssetsToPath(this, basePath, "data_v2.zip");
            File zipFile = new File(basePath, "data_v2.zip");
            if (zipFile.exists()) {
                ZipUtil decompress = new ZipUtil(this, basePath, zipFile);
                Logger.d("Unzip zip file on Card");
                decompress.unzip();
                Logger.d("Delete zip file on Card");
                zipFile.delete();
            } else {
                Logger.e("Zip file not found on Card");
                Toast.makeText(this, "Oops, something's wrong", Toast.LENGTH_SHORT).show();
            }
        }
    }
}