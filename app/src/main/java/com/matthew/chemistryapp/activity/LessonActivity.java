package com.matthew.chemistryapp.activity;

import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.PersistableBundle;
import android.support.design.widget.Snackbar;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.BounceInterpolator;
import android.view.animation.TranslateAnimation;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.matthew.chemistryapp.MyApp;
import com.matthew.chemistryapp.R;
import com.matthew.chemistryapp.adapter.ContentFragmentAdapter;
import com.matthew.chemistryapp.common.Constants;
import com.matthew.chemistryapp.data.DataManager;
import com.matthew.chemistryapp.model.Unit;
import com.matthew.chemistryapp.util.AESHelper;
import com.matthew.chemistryapp.util.FolderUtil;
import com.matthew.chemistryapp.fragment.ContentFragment;
import com.matthew.chemistryapp.lib.VerticalViewPager;
import com.matthew.chemistryapp.model.Lesson;
import com.matthew.chemistryapp.model.Slide;
import com.matthew.chemistryapp.util.ResourceUtils;
import com.orhanobut.logger.Logger;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import biz.kasual.materialnumberpicker.MaterialNumberPicker;
import icepick.Icepick;
import icepick.State;
import tourguide.tourguide.ChainTourGuide;
import tourguide.tourguide.Overlay;
import tourguide.tourguide.Pointer;
import tourguide.tourguide.Sequence;
import tourguide.tourguide.ToolTip;
import tourguide.tourguide.TourGuide;

public class LessonActivity extends AppCompatActivity implements View.OnClickListener {
    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;
    private WebView webView;
    private Lesson lesson;
    private Unit unit;
    private List<Slide> listSlides = new ArrayList<>();
    private Toolbar toolbar;
    private TextView currentSlideTextView;
    MaterialNumberPicker numberPicker;
    AlertDialog alertDialog;
    VerticalViewPager viewPager;
    Button btnNotes;

    @State
    boolean isInstructionShown = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Icepick.restoreInstanceState(this, savedInstanceState);
        setContentView(R.layout.activity_lesson);
        if (getIntent() != null) {
            unit = getIntent().getParcelableExtra(MainActivity.UNIT);
            lesson = getIntent().getParcelableExtra(MainActivity.LECTURE);
        }

        setSupportActionBar(toolbar);
        setupToolbar();
        setupCoachMarkView();

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        webView = (WebView) findViewById(R.id.webview);
        currentSlideTextView = (TextView) toolbar.findViewById(R.id.currentSlide);
        btnNotes = (Button) toolbar.findViewById(R.id.btnNotes);

        btnNotes.setOnClickListener(this);

        initDrawer();
        initWebView();


        try {
            getListSlides();
            initViewPager();

            String current = getCurrentSlideInString(0);
            SpannableString content = new SpannableString(current);
            content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
            currentSlideTextView.setText(content);
        } catch (IOException e) {
            e.printStackTrace();
            Toast.makeText(this, "Error while loading slides", Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            e.printStackTrace();
        }
        setupNumberPicker(1);
        initAlertDialog();

        currentSlideTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.show();
            }
        });
    }

    private void initAlertDialog() {
        alertDialog = null;
        alertDialog = new AlertDialog.Builder(this)
                .setTitle("Select a slide")
                .setView(numberPicker)
                .setPositiveButton(getString(android.R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        int current = numberPicker.getValue();
                        viewPager.setCurrentItem(current - 1);
                        dialog.dismiss();
                    }
                }).setNegativeButton(getString(android.R.string.cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).create();
    }

    private void setupNumberPicker(int currentValue) {
        numberPicker = new MaterialNumberPicker.Builder(this)
                .minValue(1)
                .maxValue(listSlides.size())
                .defaultValue(currentValue)
                .backgroundColor(Color.WHITE)
                .separatorColor(Color.TRANSPARENT)
                .textColor(Color.BLACK)
                .textSize(20)
                .enableFocusability(false)
                .wrapSelectorWheel(true)
                .build();
    }


    private void setupToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Lecture " + lesson.getId());
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    private void setupCoachMarkView() {
        if (!isInstructionShown) {
            isInstructionShown = true;
            final Dialog dialog = new Dialog(this);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.dialog_coach_mark);

            Button dialogButton = (Button) dialog.findViewById(R.id.gotItButton);
            // if button is clicked, close the custom dialog
            dialogButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });

            dialog.show();
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Icepick.saveInstanceState(this, outState);
    }

    private void getListSlides() throws Exception {
        String basePath = getFilesDir().getAbsolutePath();
        String slidePath = basePath + File.separator + Constants.BASE_DATA_PATH + unit.getId() + File.separator + Constants.BASE_LESSON_PATH + File.separator + lesson.getId() + Constants.CONTENT_JSON;
        InputStream contentInputStream = new FileInputStream(slidePath);
        String json = IOUtils.toString(contentInputStream, "utf-8");
        listSlides = DataManager.getSlides(json);
    }


    @Override
    public void onPostCreate(Bundle savedInstanceState, PersistableBundle persistentState) {
        super.onPostCreate(savedInstanceState, persistentState);
        mDrawerToggle.syncState();
    }

    private void initDrawer() {
        mDrawerToggle = new ActionBarDrawerToggle(
                this,
                mDrawerLayout,
                null,
                R.string.drawer_open,
                R.string.drawer_close
        ) {
            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
            }

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);
    }

    private void initViewPager() {
        viewPager = (VerticalViewPager) findViewById(R.id.vertical_viewpager);
        //viewPager.setPageTransformer(false, new ZoomOutTransformer());
        //viewPager.setPageTransformer(true, new StackTransformer());
        ContentFragmentAdapter adapter = new ContentFragmentAdapter(getSupportFragmentManager());
        for (int i = 0; i < listSlides.size(); i++) {
            Slide slide = listSlides.get(i);
            adapter.addFragment(ContentFragment.newInstance(unit, lesson, slide));
        }
        viewPager.setAdapter(adapter);
        //If you setting other scroll mode, the scrolled fade is shown from either side of display.
        viewPager.setOverScrollMode(View.OVER_SCROLL_NEVER);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                try {
                    String current = getCurrentSlideInString(position);
                    SpannableString content = new SpannableString(current);
                    content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
                    currentSlideTextView.setText(content);

                    setupNumberPicker(position + 1);
                    initAlertDialog();

                    loadNote(listSlides.get(position));
                } catch (IOException e) {
                    e.printStackTrace();
                    Logger.e(e.getMessage());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }


    private void initWebView() {
        WebSettings webSettings = this.webView.getSettings();
        String appCachePath = getApplicationContext().getCacheDir().getAbsolutePath();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setSupportZoom(false);
        webSettings.setDomStorageEnabled(true);
        webSettings.setAppCacheEnabled(true);
        webSettings.setAppCachePath(appCachePath);
        webSettings.setAllowFileAccess(true);
        webSettings.setCacheMode(-1);
        if (Build.VERSION.SDK_INT < 19) {
            String databasePath = getApplicationContext().getDir("databases", 0).getPath();
            webSettings.setDatabaseEnabled(true);
            webSettings.setDatabasePath(databasePath);
        }
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onLoadResource(WebView view, String url) {
                //progressBar.setVisibility(View.VISIBLE);
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                // progressBar.setVisibility(View.GONE);
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if (Uri.parse(url).getHost().contains(".jpg")) {
                    // This is my web site, so do not override; let my WebView load the page
                    return false;
                }
                return true;
            }
        });
        if (Build.VERSION.SDK_INT >= 19) {
            WebView.setWebContentsDebuggingEnabled(false);
        }

    }

    private void loadNote(Slide slide) throws Exception {
        loadPremiumNote(slide);
    }

    private void loadPremiumNote(Slide slide) throws Exception {
        String htmlContent;
        String baseUrl;
        String historyUrl;
        String basePath = getFilesDir().getAbsolutePath();
        if (slide.isHasNote()) {
            String htmlPath = basePath + File.separator + FolderUtil.getSlidePath(String.valueOf(unit.getId()), String.valueOf(lesson.getId()), String.valueOf(slide.getId()), "html");
            InputStream inputStream = new FileInputStream(new File(htmlPath));
            htmlContent = IOUtils.toString(inputStream, "utf-8");
            baseUrl = "file://data/data/com.matthew.chemistryapp/files/data_v2/" + unit.getId() + "/" + "lessons/" + lesson.getId() + "/" + slide.getId() + ".html";
        } else {
            String emptyHtmlPath = basePath + File.separator + FolderUtil.getEmptyPath();
            htmlContent = FileUtils.readFileToString(new File(emptyHtmlPath), "utf-8");
            baseUrl = "file://data/data/com.matthew.chemistryapp/files/data_v2/empty.html";
        }
        historyUrl = baseUrl;
        webView.loadDataWithBaseURL(baseUrl
                , htmlContent
                , "text/html"
                , "UTF-8"
                , historyUrl);

    }

    private String getCurrentSlideInString(int position) {
        int lastSlideIndex = listSlides == null ? 0 : listSlides.size();
        return (position + 1) + "/" + lastSlideIndex;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            default:
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        if (mDrawerLayout.isDrawerOpen(Gravity.RIGHT)) {
            mDrawerLayout.closeDrawers();
        } else {
            mDrawerLayout.openDrawer(Gravity.RIGHT);
        }
    }
}

