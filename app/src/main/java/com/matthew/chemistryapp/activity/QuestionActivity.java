package com.matthew.chemistryapp.activity;

import android.net.Uri;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.matthew.chemistryapp.R;
import com.matthew.chemistryapp.model.Lesson;
import com.matthew.chemistryapp.model.Unit;
import com.matthew.chemistryapp.util.FolderUtil;

import java.io.File;

public class QuestionActivity extends AppCompatActivity {
    private WebView webView;
    private Unit unit;
    private Lesson lesson;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question);
        webView = (WebView) findViewById(R.id.webview_ques);
        if (getIntent() != null) {
            unit = getIntent().getParcelableExtra(MainActivity.UNIT);
            lesson = getIntent().getParcelableExtra(MainActivity.LECTURE);
        }
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        initWebView();
        try {
                loadQuestion();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void loadQuestion() throws Exception {
        String basePath = getFilesDir().getAbsolutePath();
        String htmlPath = basePath + File.separator + FolderUtil.getLessonPath(String.valueOf(unit.getId()), String.valueOf(lesson.getId()))+".html";
        String baseUrl = "file:///data/data/com.matthew.chemistryapp/files/data_v2/" + unit.getId() + "/" + "lessons/" + lesson.getId() + ".html";
        webView.loadUrl(baseUrl);
    }

    private void initWebView() {
        WebSettings webSettings = this.webView.getSettings();
        String appCachePath = getApplicationContext().getCacheDir().getAbsolutePath();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setSupportZoom(false);
        webSettings.setDomStorageEnabled(true);
        webSettings.setAppCacheEnabled(true);
        webSettings.setAppCachePath(appCachePath);
        webSettings.setAllowFileAccess(true);
        webSettings.setCacheMode(-1);
        if (Build.VERSION.SDK_INT < 19) {
            String databasePath = getApplicationContext().getDir("databases", 0).getPath();
            webSettings.setDatabaseEnabled(true);
            webSettings.setDatabasePath(databasePath);
        }
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onLoadResource(WebView view, String url) {
                //progressBar.setVisibility(View.VISIBLE);
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                // progressBar.setVisibility(View.GONE);
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if (Uri.parse(url).getHost().contains(".jpg")) {
                    // This is my web site, so do not override; let my WebView load the page
                    return false;
                }
                return true;
            }
        });
        if (Build.VERSION.SDK_INT >= 19) {
            WebView.setWebContentsDebuggingEnabled(false);
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            default:
        }
        return super.onOptionsItemSelected(item);
    }
}
