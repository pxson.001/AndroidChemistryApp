package com.matthew.chemistryapp.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import com.matthew.chemistryapp.R;
import com.matthew.chemistryapp.common.Constants;
import com.matthew.chemistryapp.model.Lesson;
import com.matthew.chemistryapp.model.Unit;
import com.matthew.chemistryapp.util.FolderUtil;
import com.matthew.chemistryapp.util.SharePref;
import com.orhanobut.logger.Logger;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by HoangCuong on 7/18/2017.
 */
public class ItemUnitAdapter extends BaseAdapter {
    private List<Unit> objects = new ArrayList<>();

    private Context context;
    private LayoutInflater layoutInflater;
    private OnDownloadUnitListener mDownloadListener;
    private OnOpenUnitListener mOnOpenlistener;

    public ItemUnitAdapter(Context context, List<Unit> objects) {
        this.context = context;
        this.layoutInflater = LayoutInflater.from(context);
        this.objects = objects;
    }

    public void setOnDownloadListener(OnDownloadUnitListener onDownloadListener) {
        this.mDownloadListener = onDownloadListener;
    }

    public void setOnOpenlistener(OnOpenUnitListener onOpenlistener) {
        this.mOnOpenlistener = onOpenlistener;
    }

    @Override
    public int getCount() {
        return objects.size();
    }

    @Override
    public Unit getItem(int position) {
        return objects.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.item_unit, null, false);
            convertView.setTag(new ViewHolder(convertView));
        }
        initializeViews(getItem(position), (ViewHolder) convertView.getTag());
        return convertView;
    }

    private void initializeViews(final Unit unit, final ViewHolder holder) {
        //TODO implement

        holder.nameTextview.setText("Unit " + unit.getId() + ": " + unit.getName());
        holder.premiumTextview.setTextColor(unit.ispremium() ? Color.parseColor("#FF5722") : Color.parseColor("#03A9F4"));
        holder.premiumTextview.setText(unit.ispremium() ? "Premium" : "Free");
        holder.downloadButton.setVisibility(FolderUtil.isUnitExists(context, unit.getId()) ? View.INVISIBLE : View.VISIBLE);
        Logger.d("Unit %s isExist = %s", unit.getId(), String.valueOf(FolderUtil.isUnitExists(context, unit.getId())));
        boolean isPremium = unit.ispremium() && SharePref.getBoolean(context, Constants.BASE_PRODUCT_ID + unit.getId(), false);
//        boolean isPremium = true;

        if (!isPremium) {
            holder.downloadButton.setAlpha((float) 0.3);
            holder.downloadButton.setEnabled(false);
        } else {
            holder.downloadButton.setAlpha((float) 1);
            holder.downloadButton.setEnabled(true);
        }
        holder.downloadButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mDownloadListener != null) {
                    mDownloadListener.onDownload(unit, holder.downloadButton);
                }
            }
        });
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnOpenlistener != null) {
                    mOnOpenlistener.onOpen(unit);
                }
            }
        });
    }

    protected class ViewHolder {
        private View itemView;
        private TextView nameTextview;
        private TextView premiumTextview;
        private ImageButton downloadButton;

        public ViewHolder(View view) {
            itemView = view;
            nameTextview = (TextView) view.findViewById(R.id.name_textview);
            premiumTextview = (TextView) view.findViewById(R.id.premium_textview);
            downloadButton = (ImageButton) view.findViewById(R.id.download_button);
        }
    }

    public interface OnDownloadUnitListener {
        void onDownload(Unit unit, ImageButton downLoad);
    }

    public interface OnOpenUnitListener {
        void onOpen(Unit unit);
    }
}
