package com.matthew.chemistryapp.adapter;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeSet;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Button;

import com.matthew.chemistryapp.R;
import com.matthew.chemistryapp.common.Constants;
import com.matthew.chemistryapp.model.Lesson;
import com.matthew.chemistryapp.util.FolderUtil;
import com.matthew.chemistryapp.util.SharePref;
import com.orhanobut.logger.Logger;

public class ItemLessonAdapter extends BaseAdapter {
    private static final int TYPE_ITEM = 0;
    private static final int TYPE_SEPARATOR = 1;
    private static final int TYPE_MAX_COUNT = TYPE_SEPARATOR + 1;
    private TreeSet mSeparatorsSet = new TreeSet();

    private List<Lesson> objects = new ArrayList<>();
    private Context context;
    private LayoutInflater layoutInflater;
    private OnOpenLessonListener mOnOpenlistener;


    public ItemLessonAdapter(Context context) {
        this.context = context;
        this.layoutInflater = LayoutInflater.from(context);
    }

    public void addItem(Lesson item) {
        objects.add(item);
        notifyDataSetChanged();
    }

    public void addSeparatorItem(Lesson item) {
        objects.add(item);
        // save separator position
        mSeparatorsSet.add(objects.size() - 1);
        notifyDataSetChanged();
    }
    @Override
    public int getItemViewType(int position) {
        return mSeparatorsSet.contains(position) ? TYPE_SEPARATOR : TYPE_ITEM;
    }

    @Override
    public int getViewTypeCount() {
        return TYPE_MAX_COUNT;
    }

    public void setOnOpenlistener(OnOpenLessonListener onOpenlistener) {
        this.mOnOpenlistener = onOpenlistener;
    }


    @Override
    public int getCount() {
        return objects.size();
    }

    @Override
    public Lesson getItem(int position) {
        return objects.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        int type = getItemViewType(position);
        if (convertView == null) {
            holder= new ViewHolder();
            switch (type) {
                case TYPE_ITEM:
                    convertView = layoutInflater.inflate(R.layout.item_lesson, null,false);
                    holder.itemView=convertView;
                    holder.nameTextview=(TextView)convertView.findViewById(R.id.name_textview);
                    break;
                case TYPE_SEPARATOR:
                    convertView = layoutInflater.inflate(R.layout.item_header, null,false);
                    holder.itemView=convertView;
                    holder.nameTextview=(TextView)convertView.findViewById(R.id.header);
                    break;
            }
            convertView.setTag(holder);
        }else{
            holder= (ViewHolder) convertView.getTag();
        }
        initializeViews(getItem(position), holder);
        return convertView;
    }

    private void initializeViews(final Lesson lesson, final ViewHolder holder) {
        //TODO implement

        holder.nameTextview.setText(lesson.getName());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnOpenlistener != null) {
                    mOnOpenlistener.onOpen(lesson);
                }
            }
        });
    }

    protected class ViewHolder {
        public View itemView;
        public TextView nameTextview;

//        public ViewHolder(View view) {
//            itemView = view;
//           // nameTextview = (TextView) view.findViewById(R.id.name_textview);
//        }
    }

    public interface OnOpenLessonListener {
        void onOpen(Lesson lesson);
    }
}
